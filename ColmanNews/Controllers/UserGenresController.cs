﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColmanNews.Data;
using eCommerce.Models;

namespace ColmanNews.Controllers
{
    public class UserGenresController : Controller
    {
        private readonly ColmanNewsContext _context;

        public UserGenresController(ColmanNewsContext context)
        {
            _context = context;
        }

        // GET: UserGenres
        public async Task<IActionResult> Index()
        {
            var colmanNewsContext = _context.UserGenres.Include(u => u.Genre);
            return View(await colmanNewsContext.ToListAsync());
        }

        // GET: UserGenres/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userGenre = await _context.UserGenres
                .Include(u => u.Genre)
                .FirstOrDefaultAsync(m => m.Username == id);
            if (userGenre == null)
            {
                return NotFound();
            }

            return View(userGenre);
        }

        // GET: UserGenres/Create
        public IActionResult Create()
        {
            ViewData["GenreId"] = new SelectList(_context.Genres, "Id", "Name");
            return View();
        }

        // POST: UserGenres/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Username,GenreId,Counter")] UserGenre userGenre)
        {
            if (ModelState.IsValid)
            {
                _context.Add(userGenre);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["GenreId"] = new SelectList(_context.Genres, "Id", "Name", userGenre.GenreId);
            return View(userGenre);
        }

        // GET: UserGenres/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userGenre = await _context.UserGenres.FindAsync(id);
            if (userGenre == null)
            {
                return NotFound();
            }
            ViewData["GenreId"] = new SelectList(_context.Genres, "Id", "Name", userGenre.GenreId);
            return View(userGenre);
        }

        // POST: UserGenres/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Username,GenreId,Counter")] UserGenre userGenre)
        {
            if (id != userGenre.Username)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(userGenre);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserGenreExists(userGenre.Username))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GenreId"] = new SelectList(_context.Genres, "Id", "Name", userGenre.GenreId);
            return View(userGenre);
        }

        // GET: UserGenres/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userGenre = await _context.UserGenres
                .Include(u => u.Genre)
                .FirstOrDefaultAsync(m => m.Username == id);
            if (userGenre == null)
            {
                return NotFound();
            }

            return View(userGenre);
        }

        // POST: UserGenres/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var userGenre = await _context.UserGenres.FindAsync(id);
            _context.UserGenres.Remove(userGenre);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserGenreExists(string id)
        {
            return _context.UserGenres.Any(e => e.Username == id);
        }
    }
}
