﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColmanNews.Data;
using eCommerce.Models;

namespace ColmanNews.Controllers
{
    public class ErrorController : Controller
    {
     
        public IActionResult InternalError()
        {
            return View("~/Views/Error/InternalError.cshtml");
        }

        // GET: Error
        public IActionResult PageNotFound()
        {
            return View("~/Views/Error/PageNotFound.cshtml");
        }


    }
}
