﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColmanNews.Data;
using eCommerce.Models;

namespace ColmanNews.Controllers
{
    public class GenresController : Controller
    {
        private readonly ColmanNewsContext _context;

        public GenresController(ColmanNewsContext context)
        {
            _context = context;
        }

        // GET: Genres
        public async Task<IActionResult> Home()
        {
            ViewBag.reporters = _context.Users.Where(user => user.UserRole == Role.Reporter);
            ViewBag.Cities = new[] { "Jerusalem", "Tel Aviv", "Eilat", "Beit Shaan"};

            var genresToCount = _context.Articles.GroupBy(art => art.GenreId).Select(group => new { genre = group.Key, count = group.Count() }).ToDictionary(group => group.genre, group => group.count);
            ViewBag.genresToCountDictionary = genresToCount;

            var genresArticlesLeftJoin = await (from gen in _context.Genres
                                      join art in _context.Articles on gen.Id equals art.GenreId into temp
                                      from t in temp.DefaultIfEmpty()
                                      select new
                                      {
                                          Genre = gen,
                                          Article = t
                                      }).ToListAsync();
            
            var genres = new List<Genre>();

            foreach(var genreArticle in genresArticlesLeftJoin)
            {
                genres.Add(genreArticle.Genre);
            }


             return View(await _context.Genres.Include(genre => genre.Articles).ToListAsync());
        }

        // GET: Genres/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var genre = await _context.Genres
                .Include(genre => genre.Articles)
                    .ThenInclude(article => article.Image)
                .Include(genre => genre.Articles)
                    .ThenInclude(article => article.Author)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (genre == null)
            {
                return NotFound();
            }

            return View(genre);
        }

        // GET: Genres/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Genres/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] Genre genre)
        {
            if (ModelState.IsValid)
            {
                _context.Add(genre);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Home));
            }
            return View(genre);
        }

        // GET: Genres/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var genre = await _context.Genres.FindAsync(id);
            if (genre == null)
            {
                return NotFound();
            }
            return View(genre);
        }

        // POST: Genres/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] Genre genre)
        {
            if (id != genre.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(genre);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GenreExists(genre.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Home));
            }
            return View(genre);
        }

        // GET: Genres/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var genre = await _context.Genres
                .FirstOrDefaultAsync(m => m.Id == id);
            if (genre == null)
            {
                return NotFound();
            }

            return View(genre);
        }

        // POST: Genres/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var genre = await _context.Genres.FindAsync(id);
            _context.Genres.Remove(genre);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Home));
        }

        private bool GenreExists(int id)
        {
            return _context.Genres.Any(e => e.Id == id);
        }

        [HttpGet("Genres")]
        public async Task<ActionResult> GetGenres()
        {
            var genres = await _context.Genres.ToListAsync();
            return Json(genres);
        }

        [HttpGet("Genres/{articleId}")]
        public async Task<ActionResult> GetGenre(int articleId)
        {
            var article = await _context.Articles
                .Include(article => article.Genre)
                .Where(article => article.Id == articleId)
                .FirstOrDefaultAsync();
            return Json(article.Genre);
        }

        public ActionResult Search(string searchTerm, string reporterUsername, string genreId)
        {
            return RedirectToAction("SearchResults", "Articles",
                new { area = "",
                    searchTerm = searchTerm, 
                    reporterUsername = reporterUsername,
                    genreId = genreId
                });
        }
    }
}
