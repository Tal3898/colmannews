﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using eCommerce.Models;

namespace ColmanNews.Data
{
    public class ColmanNewsContext : DbContext
    {
        public ColmanNewsContext (DbContextOptions<ColmanNewsContext> options)
            : base(options)
        {
        }

        public DbSet<eCommerce.Models.User> Users { get; set; }
        public DbSet<eCommerce.Models.Genre> Genres { get; set; }
        public DbSet<eCommerce.Models.Article> Articles { get; set; }
        public DbSet<eCommerce.Models.Comment> Comments { get; set; }
        public DbSet<eCommerce.Models.UserGenre> UserGenres{ get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserGenre>().HasKey(userGenre => new { userGenre.Username, userGenre.GenreId });
        }

        public DbSet<eCommerce.Models.City> City { get; set; }

        public DbSet<eCommerce.Models.Image> Image { get; set; }
    }
}
