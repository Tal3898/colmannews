﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eCommerce.Models
{
    public class Image
    {
        [Key]

        public int Id { get; set; }
        [Required]
        public string ImageTitle { get; set; }
        [Required]
        public byte[] ImageData { get; set; }
    }
}
