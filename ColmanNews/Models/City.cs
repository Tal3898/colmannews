﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eCommerce.Models
{
    public class City
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string RealCity { get; set; }

        [Required]
        public string SmurfsCity { get; set; }
    }
}
