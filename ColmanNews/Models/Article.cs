﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eCommerce.Models
{
    public class Article
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false), Required]
        [DisplayName("Post Date")]
        public DateTime CreationDate { get; set; }

        public string AuthorUserName { get; set; }

        public User Author { get; set; }

        [DisplayName("Genre")]
        public int GenreId { get; set; }

        public virtual Genre Genre { get; set; }

        public ICollection<Comment> Comments { get; set; } = new List<Comment>();

        [DisplayName("Image")]
        [ForeignKey("Image")]
        public int? ImageId { get; set; }

        public virtual Image Image { get; set; }
    }
}
